<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::get('/alquileres', 'App\Http\Controllers\AlquileresController@index');
Route::get('/alquileres/{id}', 'App\Http\Controllers\AlquileresController@showDetail');
Route::get('/alquileres/trimestral/{trimestre}', 'App\Http\Controllers\AlquileresController@trimestre');
Route::get('/alquileres/districte/{districte}', 'App\Http\Controllers\AlquileresController@districte');
Route::get('/alquileres/barri/{barri}', 'App\Http\Controllers\AlquileresController@barri');
Route::get('/mensual', 'App\Http\Controllers\AlquileresController@mensual');
Route::get('/superficie', 'App\Http\Controllers\AlquileresController@superficie');
Route::get('/alquileres/preuMajor/{preu}', 'App\Http\Controllers\AlquileresController@preuMajor');
Route::get('/alquileres/preuMenor/{preu}', 'App\Http\Controllers\AlquileresController@preuMenor');