<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alquiler extends Model
{
    use HasFactory;
    protected $fillable = ['any', 'trimestre', 'codi_districte', 'nom_districte', 'codi_barri', 'nom_barri', 'lloguer_mitja', 'preu'];
    public $timestamps = false;
}
