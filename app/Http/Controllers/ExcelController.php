<?php

namespace App\Http\Controllers;

use App\Imports\AlquileresImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function alquilerImport() 
    {
        Excel::import(new AlquileresImport, '2021_lloguer_preu_trim.csv');
    }
}
