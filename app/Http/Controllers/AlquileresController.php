<?php

namespace App\Http\Controllers;

use App\Models\Alquiler;
use Illuminate\Http\Request;

class AlquileresController extends Controller
{
    public function index(Request $request)
    {
        $alquiler = Alquiler::all();
        return $alquiler;
    }

    public function showDetail(Request $request)
    {
        $alquiler = Alquiler::query()
            ->where('_id', $request->id)
            ->get();
        return $alquiler;
    }

    public function trimestre(Request $request)
    {
        $alquiler = Alquiler::query()
            ->where('trimestre', $request->trimestre)
            ->get();
        
        return $alquiler;
    }

    public function districte(Request $request)
    {
        $alquiler = Alquiler::query()
            ->where('nom_districte', $request->districte)
            ->get();
        return $alquiler;
    }

    public function barri(Request $request)
    {
        $alquiler = Alquiler::query()
            ->where('nom_barri', $request->barri)
            ->get();
        return $alquiler;
    }

    public function mensual(Request $request)
    {
        $alquiler = Alquiler::query()
            ->where('lloguer_mitja', 'LIKE', '%mensual%')
            ->get();

        return $alquiler;
    }

    public function superficie(Request $request)
    {
        $alquiler = Alquiler::query()
            ->where('lloguer_mitja', 'LIKE', '%superfície%')
            ->get();

        return $alquiler;
    }

    public function preuMajor(Request $request)
    {
        $alquilerNotNull = Alquiler::query()
            ->where('lloguer_mitja', 'LIKE', '%mensual%')
            ->where('preu', '!=', '--')
            ->get();

        $alquiler = [];
        foreach ($alquilerNotNull as $row) {
            if (floatval($request->preu) <= floatval($row->preu)) {
                array_push($alquiler, $row);
            }
        }

        return $alquiler;
    }

    public function preuMenor(Request $request)
    {
        $alquilerNotNull = Alquiler::query()
            ->where('lloguer_mitja', 'LIKE', '%mensual%')
            ->where('preu', '!=', '--')
            ->get();

        $alquiler = [];
        foreach ($alquilerNotNull as $row) {
            if (floatval($request->preu) >= floatval($row->preu)) {
                array_push($alquiler, $row);
            }
        }

        return $alquiler;
    }
}
