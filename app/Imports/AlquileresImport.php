<?php

namespace App\Imports;

use App\Models\Alquiler;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class AlquileresImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach($collection as $row) {
            if ($row[0] != 'Any') {
                $ok = Alquiler::create([
                    'any' => $row[0],
                    'trimestre' => $row[1],
                    'codi_districte' => $row[2],
                    'nom_districte' => $row[3],
                    'codi_barri' => $row[4],
                    'nom_barri' => $row[5],
                    'lloguer_mitja' => $row[6],
                    'preu' => $row[7],
                ]);
            }
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
