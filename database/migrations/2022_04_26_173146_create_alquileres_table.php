<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlquileresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alquilers', function (Blueprint $table) {
            $table->increments('_id');
            $table->integer('any');
            $table->integer('trimestre');
            $table->integer('codi_districte');
            $table->string('nom_districte', 100);
            $table->integer('codi_barri');
            $table->string('nom_barri', 100);
            $table->string('lloguer_mitja', 100);
            $table->string('preu', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alquilers');
    }
}
